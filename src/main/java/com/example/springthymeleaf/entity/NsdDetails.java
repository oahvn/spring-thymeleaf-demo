package com.example.springthymeleaf.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.springthymeleaf.dao.RoleDao;

public class NsdDetails implements UserDetails {
	private static final long serialVersionUID = 1L;

	@Autowired
	private RoleDao dao;

	private HtNsd user;

	public NsdDetails(HtNsd user) {
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<HtNhom> htNhoms = dao.findByNsdId(user.getId());
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		if (!htNhoms.isEmpty()) {
			for (HtNhom i : htNhoms) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + i.getMa().toUpperCase()));
			}
		}
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return user.getMatKhau();
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return user.getTenDangNhap();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
