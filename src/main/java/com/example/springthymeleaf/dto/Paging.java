package com.example.springthymeleaf.dto;

import ulti.Pageable;

public class Paging {
	private NsdSearchForm similar = new NsdSearchForm();
	private Pageable pageable = new Pageable();

	public Paging() {
		super();
	}

	public Paging(NsdSearchForm similar, Pageable pageable) {
		super();
		this.similar = similar;
		this.pageable = pageable;
	}

	public NsdSearchForm getSimilar() {
		return similar;
	}

	public void setSimilar(NsdSearchForm similar) {
		this.similar = similar;
	}

	public Pageable getPageable() {
		return pageable;
	}

	public void setPageable(Pageable pageable) {
		this.pageable = pageable;
	}

}
