package com.example.springthymeleaf.dto;

import java.util.List;

import com.example.springthymeleaf.entity.HtNsd;

public class SearchResponse {
	private List<HtNsd> nsds;
	private Paging paging;

	public SearchResponse() {
		super();
	}

	public SearchResponse(List<HtNsd> nsds, Paging paging) {
		super();
		this.nsds = nsds;
		this.paging = paging;
	}

	public List<HtNsd> getNsds() {
		return nsds;
	}

	public void setNsds(List<HtNsd> nsds) {
		this.nsds = nsds;
	}

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

}
