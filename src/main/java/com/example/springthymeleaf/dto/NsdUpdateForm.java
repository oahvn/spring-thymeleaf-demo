package com.example.springthymeleaf.dto;

import java.util.ArrayList;
import java.util.List;

import com.example.springthymeleaf.entity.HtNhom;
import com.example.springthymeleaf.entity.HtNsd;

public class NsdUpdateForm {
	private String id;
	private String tenDangNhap;
	private String matKhau;
	private String email;
	private String dienThoai;
	List<String> roleId = new ArrayList<>(0);

	public NsdUpdateForm(HtNsd htNsd, List<HtNhom> roles) {
		this.tenDangNhap = htNsd.getTenDangNhap();
		this.id = htNsd.getId();
		this.matKhau = htNsd.getMatKhau();
		this.email = htNsd.getEmail();
		this.dienThoai = htNsd.getDienThoai();
		if (roles != null && !roles.isEmpty()) {
			for (HtNhom role : roles) {
				this.roleId.add(role.getId());
			}
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public String getMatKhau() {
		return matKhau;
	}

	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDienThoai() {
		return dienThoai;
	}

	public void setDienThoai(String dienThoai) {
		this.dienThoai = dienThoai;
	}

	public List<String> getRoleId() {
		return roleId;
	}

	public void setRoleId(List<String> roleId) {
		this.roleId = roleId;
	}

}
