package com.example.springthymeleaf.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class NsdSearchForm {
	private String tenDangNhap;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date ngayTao;

	public NsdSearchForm() {
		super();
	}

	public NsdSearchForm(String tenDangNhap, Date ngayTao) {
		super();
		this.tenDangNhap = tenDangNhap;
		this.ngayTao = ngayTao;
	}

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public Date getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Date ngayTao) {
		this.ngayTao = ngayTao;
	}

}
