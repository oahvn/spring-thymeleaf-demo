package com.example.springthymeleaf.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.springthymeleaf.dto.NsdUpdateForm;
import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.service.RoleService;
import com.example.springthymeleaf.service.UserService;

import ulti.Pageable;

@Controller
public class UserController {

	@Autowired
	private UserService service;
	@Autowired
	private RoleService roleService;

	@GetMapping(value = { "/index/{page}/{total}" })
	public String index(@PathVariable("page") Integer page, @PathVariable("total") Integer total, Model model) {
		if (page == null) {
			page = 1;
		}
		if (total == null) {
			total = 1;
		}
////		List<HtNsd> list = service.paging(null, null, new Pageable(page, total, 0));
////		Integer count = service.count(null, null);
//		int totalPages = (count % total == 0) ? count / total : count / total + 1;
//		model.addAttribute("page", page);
//		model.addAttribute("total", total);
//		model.addAttribute("pagination", pagination(page, totalPages));
//		model.addAttribute("nsd", new HtNsd());
//		model.addAttribute("nsds", list);
		return "nsd";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute(name = "nsd") @Valid HtNsd nsd, BindingResult result) {
		if (result.hasFieldErrors()) {
			return "nsd";
		}
//		service.save(nsd);
		return "redirect:index";
	}

	@GetMapping("/detail/{id}")
	public String detail(@PathVariable("id") String id, Model model) {
		if (id != null && !id.trim().equals("")) {
			HtNsd htNsd = service.findById(id);
			if (htNsd != null) {
//				List<HtNhom> roles = roleService;
//				List<HtNhom> htNhoms = roleService.findRoleByNsdId(id);
				NsdUpdateForm formUpdate = new NsdUpdateForm(htNsd, null);
				model.addAttribute("nsd", formUpdate);
				return "detail";
			}
			return "index";
		} else {
			return "index";
		}
	}

	@PostMapping("/update")
	public String update(@ModelAttribute("nsd") NsdUpdateForm item) {
		return null;
	}

	public List<Integer> pagination(int page, long totalPages) {
		List<Integer> paginations = new ArrayList<Integer>();
		if (totalPages < 5) {
			for (int i = 1; i <= totalPages; i++) {
				paginations.add(i);
			}
		} else {
			if (page >= 3 && page < totalPages - 2) {
				for (int i = page - 2; i <= page + 2; i++) {
					paginations.add(i);
				}
			} else if (page < 3) {
				for (int i = 1; i <= 5; i++) {
					paginations.add(i);
				}
			} else {
				for (int i = (int) (totalPages - 4); i <= totalPages; i++) {
					paginations.add(i);
				}
			}
		}
		return paginations;
	}
}
