package com.example.springthymeleaf.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.example.springthymeleaf.dto.NsdSearchForm;
import com.example.springthymeleaf.dto.Paging;
import com.example.springthymeleaf.dto.SearchResponse;
import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.myservice.NsdService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ulti.Pageable;

@Controller
public class TestController {

	private String URL = "http://localhost:8080/api/nsd";
	@Autowired
	private NsdService service;

	@GetMapping("/danh-sach-nsd/{page}/{total}")
	public String danhSachNSD(@PathVariable("page") int page, @PathVariable("total") int total, Model model,
			HttpSession session) {
		NsdSearchForm nsdSearchForm = (NsdSearchForm) session.getAttribute("nsdSearchForm");
		Paging paging = null;
		if (nsdSearchForm == null) {
			paging = new Paging(new NsdSearchForm(), new Pageable(page, total, 0));
			model.addAttribute("nsdSearchForm", new NsdSearchForm("", null));
		} else {
			paging = new Paging(nsdSearchForm, new Pageable(page, total, 0));
			model.addAttribute("nsdSearchForm", nsdSearchForm);
		}
		SearchResponse response = service.findNsd(paging);

		if (response != null) {
			model.addAttribute("nsds", response.getNsds());
			List<Integer> pagination = pagination(page, response.getPaging().getPageable().getTotalPages());
			model.addAttribute("pageable", response.getPaging().getPageable());
			model.addAttribute("pagination", pagination);
		}
		return "danh-sach-nsd";
	}

	@PostMapping("/tim-kiem")
	public String timKiem(@ModelAttribute("nsdSearchForm") NsdSearchForm search, HttpSession session) {
		session.setAttribute("nsdSearchForm", search);
		return "redirect:danh-sach-nsd/1/5";
	}

	@RequestMapping("/huy-tim-kiem")
	public String huyTimKiem(HttpSession session) {
		session.removeAttribute("nsdSearchForm");
		return "redirect:/danh-sach-nsd/1/5";
	}

	@GetMapping("/them-moi-nsd")
	public String themMoiNSD(Model model) {
		HtNsd nsd = new HtNsd();
		model.addAttribute("nsd", nsd);
		return "them-moi-nsd";
	}

	@GetMapping("/cap-nhat-nsd/{id}")
	public String themMoiNSD(@PathVariable("id") String id, Model model) {
		HtNsd nsd = service.findById(id);
		model.addAttribute("isUpdate", true);
		model.addAttribute("nsd", nsd);
		return "them-moi-nsd";
	}

	@PostMapping("/them-moi-nsd-submit")
	public String themMoiNSDSubmit(@ModelAttribute("nsd") @Valid HtNsd nsd, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("nsd", nsd);
			return "them-moi-nsd";
		}
		Object resultSave = service.save(nsd);
		if (resultSave == null) {
			model.addAttribute("nsd", new HtNsd());
			return "redirect:/them-moi-nsd?status=successed";
		} else {
			model.addAttribute("nsd", nsd);
			model.addAttribute("error", resultSave.toString());
			return "them-moi-nsd";
		}
	}

	@PostMapping("/cap-nhat-nsd-submit")
	public String capNhatNSDSubmit(@ModelAttribute("nsd") @Valid HtNsd nsd, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("nsd", nsd);
			return "redirect:cap-nhat-nsd/" + nsd.getId() + "?status=errors";
		}
		service.update(nsd);
		model.addAttribute("nsd", new HtNsd());
		return "redirect:cap-nhat-nsd/" + nsd.getId() + "?status=successed";
	}

	@GetMapping("/xoa-nsd/{id}")
	public String xoaNSD(@PathVariable("id") String id) {
		if (id == null) {
			return "redirect:/danh-sach-nsd/1/5";
		} else {
			service.delete(id);
			return "redirect:/danh-sach-nsd/1/5";

		}
	}

	@GetMapping("/nsd")
	public String index() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<HtNsd[]> entity = new HttpEntity<HtNsd[]>(headers);

		URL += "/" + "1";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<HtNsd> responseEntity = restTemplate.exchange(URL, HttpMethod.GET, entity, HtNsd.class);

		HtNsd data = responseEntity.getBody();// restTemplate.getForEntity(URL,
												// HtNsd.class).getBody();
		System.out.println(data);
		return "success";
	}

	@GetMapping("/search/{page}")
	public String search(@PathVariable int page) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
		headers.setContentType(MediaType.APPLICATION_JSON);

		NsdSearchForm search = new NsdSearchForm(null, null);
		Paging paging = new Paging(search, new Pageable(page, 1, 0));

		HttpEntity<Paging> entity = new HttpEntity<>(paging);

		String url = "http://localhost:8080/api/nsd/search";
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<SearchResponse> responseEntity = restTemplate.postForEntity(url, entity, SearchResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getBody()));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "success";
	}

	private List<Integer> pagination(int page, long totalPages) {
		List<Integer> paginations = new ArrayList<Integer>();
		if (totalPages < 5) {
			for (int i = 1; i <= totalPages; i++) {
				paginations.add(i);
			}
		} else {
			if (page >= 3 && page < totalPages - 2) {
				for (int i = page - 2; i <= page + 2; i++) {
					paginations.add(i);
				}
			} else if (page < 3) {
				for (int i = 1; i <= 5; i++) {
					paginations.add(i);
				}
			} else {
				for (int i = (int) (totalPages - 4); i <= totalPages; i++) {
					paginations.add(i);
				}
			}
		}
		return paginations;
	}

}
