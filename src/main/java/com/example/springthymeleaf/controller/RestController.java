package com.example.springthymeleaf.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.springthymeleaf.dto.Paging;
import com.example.springthymeleaf.dto.SearchResponse;
import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.exception.ExistingUsernameException;
import com.example.springthymeleaf.service.UserService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/nsd")
public class RestController {

	@Autowired
	private UserService service;
	private List<String> messages;

	private ResponseEntity<Object> validate(BindingResult result) {
		messages = new ArrayList<String>();
		result.getAllErrors().forEach(error -> {
			messages.add(error.getDefaultMessage());
		});
		return new ResponseEntity<Object>(messages, HttpStatus.BAD_REQUEST);

	}

	@GetMapping("/{id}")
	public ResponseEntity<HtNsd> findById(@PathVariable("id") String id) {
		HtNsd nsd = service.findById(id);
		if (nsd == null) {
			return new ResponseEntity<HtNsd>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<HtNsd>(nsd, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestBody @Valid HtNsd nsd, BindingResult resultValidate) {
		if (resultValidate.hasErrors()) {
			return validate(resultValidate);
		}
		messages = new ArrayList<String>();
		HtNsd result;
		try {
			result = service.save(nsd);
		} catch (NullPointerException e) {
			messages.add(e.getMessage());
			return new ResponseEntity<Object>(messages, HttpStatus.BAD_REQUEST);
		} catch (ExistingUsernameException e) {
			messages.add(e.getMessage());
			return new ResponseEntity<Object>(messages, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}

	@PostMapping("/update")
	public ResponseEntity<Object> update(@RequestBody @Valid HtNsd nsd, BindingResult result) {
		if (result.hasErrors()) {
			return validate(result);
		}
		messages = new ArrayList<String>();
		HtNsd rs;
		try {
			rs = service.update(nsd);
		} catch (NullPointerException e) {
			messages.add(e.getMessage());
			return new ResponseEntity<Object>(messages, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Object>(rs, HttpStatus.OK);
	}

	@GetMapping("/delete/{id}")
	public HttpStatus delete(@PathVariable String id) {
		try {
			service.delete(id);
		} catch (NullPointerException e) {
			e.printStackTrace();
			return HttpStatus.BAD_REQUEST;
		}
		return HttpStatus.OK;
	}

	@GetMapping("/paging")
	public Paging getPaging() {
		return new Paging();
	}

	@PostMapping("/search")
	public ResponseEntity<?> search(@RequestBody(required = false) Paging paging) {
		List<HtNsd> nsds = service.paging(paging.getSimilar(), paging.getPageable());
		Integer records = service.count(paging.getSimilar());
		int totalPages = (records % paging.getPageable().getTotal() == 0) ? //
				records / paging.getPageable().getTotal() : //
				records / paging.getPageable().getTotal() + 1;//
		paging.getPageable().setTotalPages(totalPages);
		SearchResponse response = new SearchResponse(nsds, paging);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

}
