package com.example.springthymeleaf.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.springthymeleaf.dto.NsdSearchForm;
import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.exception.ExistingUsernameException;

import ulti.Pageable;

@Service
public interface UserService {
	public boolean login(String username, String password);
	public HtNsd findByTenDangNhap(String tenDangNhap);
	public List<HtNsd> paging(NsdSearchForm similar, Pageable pageable);
	public Integer count(NsdSearchForm similar);
	
	public HtNsd findById(String id) throws NullPointerException;
	public HtNsd save(HtNsd nsd) throws NullPointerException,ExistingUsernameException;
	public HtNsd update(HtNsd nsd) throws NullPointerException;
	public String delete(String id) throws NullPointerException;
}
