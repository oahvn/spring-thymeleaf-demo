package com.example.springthymeleaf.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.springthymeleaf.entity.HtNhom;

@Service
public interface RoleService {
	List<HtNhom> findRoleByNsdId(String nsdId);
}
