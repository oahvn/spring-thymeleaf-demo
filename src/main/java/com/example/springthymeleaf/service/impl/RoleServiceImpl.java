package com.example.springthymeleaf.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springthymeleaf.dao.RoleDao;
import com.example.springthymeleaf.entity.HtNhom;
import com.example.springthymeleaf.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao dao;

	@Override
	public List<HtNhom> findRoleByNsdId(String nsdId) {
		return dao.findByNsdId(nsdId);
	}
}