package com.example.springthymeleaf.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.springthymeleaf.dao.UserDao;
import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.entity.NsdDetails;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserDao dao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		HtNsd nsd = dao.findByTenDangNhap(username);
		if (nsd == null) {
			System.out.println("User not found! " + username);
			throw new UsernameNotFoundException("User " + username + " was not found in the database");
		}else{
			NsdDetails details = new NsdDetails(nsd);
			return details;
		}
	}

}
