package com.example.springthymeleaf.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springthymeleaf.dao.UserDao;
import com.example.springthymeleaf.dto.NsdSearchForm;
import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.exception.ExistingUsernameException;
import com.example.springthymeleaf.service.UserService;

import ulti.Pageable;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	@Override
	public boolean login(String username, String password) {
		return false;
	}

	@Override
	public HtNsd findByTenDangNhap(String tenDangNhap) {
		return dao.findByTenDangNhap(tenDangNhap);
	}

	@Override
	public List<HtNsd> paging(NsdSearchForm similar, Pageable pageable) {
		return dao.paging(similar, pageable);
	}

	@Override
	public Integer count(NsdSearchForm similar) {
		return dao.count(similar);
	}

	@Override
	public HtNsd findById(String id) {
		if (id == null || id.trim().equals("")) {
			return null;
		}
		return dao.findById(id);
	}

	@Override
	public HtNsd save(HtNsd nsd) throws NullPointerException, ExistingUsernameException {

		if (nsd == null)
			throw new NullPointerException("Can't save nsd is null");

		HtNsd exists = this.findByTenDangNhap(nsd.getTenDangNhap());
		if (exists != null)
			throw new ExistingUsernameException("Existing username");

		nsd.setId(UUID.randomUUID().toString());
		nsd.setNgayTao(new Date(System.currentTimeMillis()));
		dao.save(nsd);
		return nsd;
	}

	@Override
	public HtNsd update(HtNsd nsd) throws NullPointerException {
		if(nsd==null){
			throw new NullPointerException("Can't update nsd is null");
		}
		nsd.setNgayCapNhat(new Date());
		dao.update(nsd);
		return this.findById(nsd.getId());
	}

	@Override
	public String delete(String id) throws NullPointerException {
		if (id == null || id.trim().equals("")) {
			throw new NullPointerException("HtNsd.id is null");
		}
		dao.delete(id);
		return id;
	}

}
