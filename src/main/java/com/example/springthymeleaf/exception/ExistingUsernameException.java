package com.example.springthymeleaf.exception;

public class ExistingUsernameException extends Exception {
	private static final long serialVersionUID = 1L;

	public ExistingUsernameException(String message) {
		super(message);
	}
}
