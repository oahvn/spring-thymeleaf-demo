package com.example.springthymeleaf.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.springthymeleaf.dto.NsdSearchForm;
import com.example.springthymeleaf.entity.HtNhom;
import com.example.springthymeleaf.entity.HtNsd;

import ulti.Pageable;

@Repository
@Transactional
public interface UserDao {
	HtNsd findById(String id);
	int save(HtNsd nsd);
	int update(HtNsd nsd);
	int delete(String id);
	
	HtNsd findByTenDangNhap(String tenDangNhap);
	List<HtNhom> findNhomsByTenDangNhap(String tenDangNhap);
	boolean findByTenDangNhapAndMatKhau(String tenDangNhap, String matKhau);
	List<HtNsd> paging(NsdSearchForm similar,Pageable pageable);
	Integer count(NsdSearchForm similar);
}
