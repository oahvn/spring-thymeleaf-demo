package com.example.springthymeleaf.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.springthymeleaf.entity.HtNhom;

@Repository
public interface RoleDao {
	List<HtNhom> findByNsdId(String nsdId);  
	List<HtNhom> findAll();
}
