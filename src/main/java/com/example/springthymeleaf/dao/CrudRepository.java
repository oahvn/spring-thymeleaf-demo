package com.example.springthymeleaf.dao;

public interface CrudRepository<T>{
	public void create(T item);
	public void read(String id);
	public void update(T item);
	public void delete(String id);
}
