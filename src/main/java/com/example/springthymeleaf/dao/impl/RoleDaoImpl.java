package com.example.springthymeleaf.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.springthymeleaf.dao.RoleDao;
import com.example.springthymeleaf.entity.HtNhom;

@Repository
@Transactional
public class RoleDaoImpl implements RoleDao{

	@Autowired
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HtNhom> findByNsdId(String nsdId) {
		List<HtNhom> htNhoms = new ArrayList<HtNhom>();
	
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("findRole",HtNhom.class)
				.registerStoredProcedureParameter("nsd_id", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("pointer", void.class, ParameterMode.REF_CURSOR);
		procedureQuery.setParameter("nsd_id", nsdId).execute();
		htNhoms = procedureQuery.getResultList();
		return htNhoms;
	}

	@Override
	public List<HtNhom> findAll() {
		List<HtNhom> list = new ArrayList<>();
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("");
		return null;
	}

}
