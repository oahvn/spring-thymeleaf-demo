package com.example.springthymeleaf.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.springthymeleaf.dao.UserDao;
import com.example.springthymeleaf.dto.NsdSearchForm;
import com.example.springthymeleaf.entity.HtNhom;
import com.example.springthymeleaf.entity.HtNsd;

import ulti.Pageable;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

	@Autowired
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public HtNsd findById(String id) {
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("HTNSD.PRO_findById", HtNsd.class)
				.registerStoredProcedureParameter("v_id", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("c_findById", void.class, ParameterMode.REF_CURSOR);
		procedureQuery.setParameter("v_id", id);
		List<HtNsd> result = procedureQuery.getResultList();
		if (!result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public boolean findByTenDangNhapAndMatKhau(String tenDangNhap, String matKhau) {
		StoredProcedureQuery procedureQuery = em.createNamedStoredProcedureQuery(HtNsd.NamedQuery_login);
		procedureQuery.setParameter("username", tenDangNhap);
		procedureQuery.setParameter("pass", matKhau);
		procedureQuery.execute();
		Integer result = (Integer) procedureQuery.getOutputParameterValue("status");
		return (result == 0) ? false : true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HtNsd findByTenDangNhap(String tenDangNhap) {
		StoredProcedureQuery procedureQuery = em//
				.createStoredProcedureQuery("HTNSD.PRO_findByTenDangNhap", HtNsd.class)//
				.registerStoredProcedureParameter("v_tenDangNhap", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("c_list", void.class, ParameterMode.REF_CURSOR);
		procedureQuery.setParameter("v_tenDangNhap", tenDangNhap);
		procedureQuery.execute();
		List<HtNsd> list = procedureQuery.getResultList();
		if (list.isEmpty()) {
			return null;
		}
		HtNsd nsd = list.get(0);
		// nsd.getHtNhomNsdsForHtNsdId().clear();
		return nsd;
	}

	@Override
	public List<HtNhom> findNhomsByTenDangNhap(String tenDangNhap) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(HtNsd nsd) {
		StoredProcedureQuery procedureQuery = em//
				.createStoredProcedureQuery("HTNSD.PRO_SAVE")
				.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(8, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(9, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(10, Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter(11, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(12, Date.class, ParameterMode.IN)
				.registerStoredProcedureParameter(13, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(14, Date.class, ParameterMode.IN)
				.registerStoredProcedureParameter(15, String.class, ParameterMode.IN);
		procedureQuery.setParameter(1, UUID.randomUUID().toString()).setParameter(2, nsd.getDmKbnnId())//
				.setParameter(3, nsd.getDmKbnnPbId())//
				.setParameter(4, nsd.getDmKbnnChucvuId())//
				.setParameter(5, nsd.getDmKbnnSudungId())//
				.setParameter(6, nsd.getTenDangNhap())//
				.setParameter(7, nsd.getMatKhau())//
				.setParameter(8, nsd.getEmail())//
				.setParameter(9, nsd.getDienThoai())//
				.setParameter(10, nsd.getLdap())//
				.setParameter(11, nsd.getTrangThai())//
				.setParameter(12, nsd.getNgayTao())//
				.setParameter(13, nsd.getNguoiTao())//
				.setParameter(14, nsd.getNgayCapNhat())//
				.setParameter(15, nsd.getNguoiCapNhat());//
		return procedureQuery.executeUpdate();
	}

	@Override
	public int update(HtNsd nsd) {
		StoredProcedureQuery procedureQuery = em//
				.createStoredProcedureQuery("HTNSD.PRO_UPDATE")//
				.registerStoredProcedureParameter("v_id", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("v_matKhau", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("v_email", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("v_dienThoai", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("v_ngayCapNhat", Date.class, ParameterMode.IN)
				.setParameter("v_id", nsd.getId())//
				.setParameter("v_matKhau", nsd.getMatKhau())//
				.setParameter("v_email", nsd.getEmail())//
				.setParameter("v_dienThoai", nsd.getDienThoai())//
				.setParameter("v_ngayCapNhat", nsd.getNgayCapNhat());
		return procedureQuery.executeUpdate();
	}

	@Override
	public int delete(String id) {
		return em.createStoredProcedureQuery("HTNSD.PRO_DELETE")//
				.registerStoredProcedureParameter("v_id", String.class, ParameterMode.IN)//
				.setParameter("v_id", id).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HtNsd> paging(NsdSearchForm similar, Pageable pageable) {
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("HTNSD.PRO_PAGING", HtNsd.class)
				.registerStoredProcedureParameter("tenDangNhap", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("ngayTao", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("currentPage", Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter("total", Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter("pointer", void.class, ParameterMode.REF_CURSOR);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		procedureQuery.setParameter("tenDangNhap", similar.getTenDangNhap())
				.setParameter("ngayTao", similar.getNgayTao() == null ? null : dateFormat.format(similar.getNgayTao()))
				.setParameter("currentPage", pageable.getPage()).setParameter("total", pageable.getTotal());
		return procedureQuery.getResultList();
	}

	@Override
	public Integer count(NsdSearchForm similar) {
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("HTNSD.PRO_COUNT")
				.registerStoredProcedureParameter("tenDangNhap", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("ngayTao", String.class, ParameterMode.IN)
				.registerStoredProcedureParameter("recs", Integer.class, ParameterMode.OUT);
		if (similar != null) {
			if (similar.getTenDangNhap()!=null && !similar.getTenDangNhap().equals(""))
				procedureQuery.setParameter("tenDangNhap", similar.getTenDangNhap());//
			procedureQuery.setParameter("ngayTao",
					similar.getNgayTao() == null ? null
							: similar.getNgayTao().toString().equals("") ? null
									: new SimpleDateFormat("dd-MM-yyyy").format(similar.getNgayTao()));
		}
		return (Integer) procedureQuery.getOutputParameterValue("recs");
	}

}
