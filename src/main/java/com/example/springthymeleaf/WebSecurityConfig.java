package com.example.springthymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.example.springthymeleaf.service.impl.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		// Sét đặt dịch vụ để tìm kiếm User trong Database.
		// Và sét đặt PasswordEncoder.
		auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.httpBasic().disable();
//		http.authorizeRequests().antMatchers("/", "/login").permitAll();
//		// http.authorizeRequests().antMatchers("").access("");
//		// http.authorizeRequests().antMatchers("").access("");
//		http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");
//
//		http.authorizeRequests().and().formLogin().loginProcessingUrl("/login-check").loginPage("/login")
//				.defaultSuccessUrl("/success").failureUrl("/login?error=true").usernameParameter("username")
//				.passwordParameter("password").and().logout().logoutUrl("/logout").logoutSuccessUrl("/login");
//
//		http.authorizeRequests().and().rememberMe().tokenRepository(this.persistentTokenRepository())
//				.tokenValiditySeconds(1800);

	}

	// Token stored in Table (Persistent_Logins)
	// @Bean
	// public PersistentTokenRepository persistentTokenRepository() {
	// JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
	// db.setDataSource(this.dataSource);
	// return db;
	// }

	// Token stored in Memory (Of Web Server).
	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		InMemoryTokenRepositoryImpl memory = new InMemoryTokenRepositoryImpl();
		return memory;
	}
}
