package com.example.springthymeleaf;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import ulti.Pageable;

@SpringBootApplication
public class SpringThymeleafApplication//implements CommandLineRunner 
{

	public static void main(String[] args) {
		SpringApplication.run(SpringThymeleafApplication.class, args);
	}

//	@Autowired
//	private UserService service;
//
//	@Override
//	public void run(String... args) throws Exception {
////		System.out.println(service.login("admin", "12345"));
//		ObjectMapper map = new ObjectMapper();
////		System.out.println(map.writerWithDefaultPrettyPrinter().writeValueAsString(service.findByTenDangNhap("admin")));
//		System.out.println(map.writerWithDefaultPrettyPrinter().writeValueAsString(service.paging(null,null, new Pageable(1, 10, 0))));
////		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
////		Date date = dateFormat.parse("27-12-2019");
////		System.out.println(service.count(null,date));
////		HtNsd nsd = new HtNsd();
////		nsd.setTenDangNhap("nvhao398");
////		nsd.setMatKhau("oahoah");
////		nsd.setNgayTao(new Date());
////		service.save(nsd);
//		System.exit(1);
//	}

}
