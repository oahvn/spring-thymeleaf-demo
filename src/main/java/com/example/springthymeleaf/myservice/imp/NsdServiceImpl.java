package com.example.springthymeleaf.myservice.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.example.springthymeleaf.dto.Paging;
import com.example.springthymeleaf.dto.SearchResponse;
import com.example.springthymeleaf.entity.HtNsd;
import com.example.springthymeleaf.myservice.NsdService;

@Service
public class NsdServiceImpl implements NsdService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public SearchResponse findNsd(Paging paging) {

		HttpEntity<Paging> entity = new HttpEntity<>(paging);
		// RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<SearchResponse> responseEntity = restTemplate.postForEntity(URL_SEARCH, entity,
				SearchResponse.class);
		SearchResponse response = responseEntity.getBody();
		return response;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public <T> T save(HtNsd nsd) {
		HttpEntity<HtNsd> entity = new HttpEntity<HtNsd>(nsd);
		try {
			ResponseEntity<HtNsd> responseEntity = restTemplate.postForEntity(URL_SAVE, entity, HtNsd.class);
		} catch (RestClientException e) {
			return (T) "Tài khoản đã tồn tại";
		}
		return null;
	}

	@Override
	public void update(HtNsd nsd) {
		HttpEntity<HtNsd> entity = new HttpEntity<HtNsd>(nsd);
		restTemplate.postForEntity(URL_UPDATE, entity, HtNsd.class);
	}

	@Override
	public void delete(String id) {
		restTemplate.getForEntity(URL_DELETE + "/" + id, String.class);
	}

	@Override
	public HtNsd findById(String id) {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<HtNsd> entity = new HttpEntity<HtNsd>(headers);
		ResponseEntity<HtNsd> responseEntity = restTemplate.getForEntity(URL_FIND_BY_ID+"/"+id, HtNsd.class);
		HtNsd nsd = responseEntity.getBody();
		return nsd;
	}

}
