package com.example.springthymeleaf.myservice;

import org.springframework.stereotype.Service;

import com.example.springthymeleaf.dto.Paging;
import com.example.springthymeleaf.dto.SearchResponse;
import com.example.springthymeleaf.entity.HtNsd;

@Service
public interface NsdService {
	String URL_FIND_BY_ID = "http://localhost:8080/api/nsd";
	String URL_SEARCH = "http://localhost:8080/api/nsd/search";
	String URL_SAVE = "http://localhost:8080/api/nsd/save";
	String URL_UPDATE = "http://localhost:8080/api/nsd/update";
	String URL_DELETE = "http://localhost:8080/api/nsd/delete";
	public HtNsd findById(String id);
	public SearchResponse findNsd(Paging paging);
	public <T> T save(HtNsd nsd);
	public void update(HtNsd nsd);
	public void delete(String id);
}
