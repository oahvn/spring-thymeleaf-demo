package ulti;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.springthymeleaf.entity.HtNsd;

public class Lazy {
	public <T> void ren(Class<T> clazz){
		Field[] fields = clazz.getDeclaredFields();
		for(Field field : fields){
			Type typeClass = field.getGenericType();
			String fieldName = field.getName();
			if(typeClass.getTypeName().contains("String")){
				System.out.println(fieldName + " in " + "varchar2,");
			}else if(typeClass.getTypeName().contains("Date")){
				System.out.println(fieldName + " in " + "Date,");
			}else {
				System.out.println(fieldName + " in " + ",");
			}
		}
	}
	
	public static void main(String[] args) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			System.out.println(dateFormat.parse("04-03-2020"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
