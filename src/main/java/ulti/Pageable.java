package ulti;

public class Pageable {
	private int page;
	private int total;
	private int totalPages;

	public Pageable() {
		super();
	}

	public Pageable(int page, int total, int totalPages) {
		super();
		this.page = page;
		this.total = total;
		this.totalPages = totalPages;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

}
